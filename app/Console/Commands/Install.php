<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Option;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Building Barbeque");
        $bar = $this->output->createProgressBar(3);
        
        $option_check   = Option::where('option','signature')->first();
        if($option_check){
            $name = $this->confirm('Application is already installed. Do you wish to reinstall system configurations and reset default Super Admin?: ');
        }
        $name                   = $this->ask('Application Name : ');
        $superadmin_password    = $this->ask('Super Admin Password : ');
        $system_default_colors  = $this->choice('System Theme',['primary', 'dark', 'success','danger']);
        $name                   = Option::updateorcreate(['option'=>'name','value'=>$name]);
        $bar->advance();
        $system_default_colors  = Option::updateorcreate(['option'=>'theme','value'=>$system_default_colors]);
        $bar->advance();
        $superadmin_password    = User::updateorcreate(['name'=>'Super Administrator','username'=>'Super Admin','password'=>Hash::make($superadmin_password)]);
        $bar->advance();
        $this->info("System Installation Completed");
    }
}
