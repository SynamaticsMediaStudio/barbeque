<?php 
use App\Option;

function option($option,$default=null){
    $option = Option::where('option',$option)->first();
    return $option? $option->value:$default;
}