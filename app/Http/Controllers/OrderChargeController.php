<?php

namespace App\Http\Controllers;

use App\OrderCharge;
use Illuminate\Http\Request;

class OrderChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderCharge  $orderCharge
     * @return \Illuminate\Http\Response
     */
    public function show(OrderCharge $orderCharge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderCharge  $orderCharge
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderCharge $orderCharge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderCharge  $orderCharge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderCharge $orderCharge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderCharge  $orderCharge
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderCharge $orderCharge)
    {
        //
    }
}
